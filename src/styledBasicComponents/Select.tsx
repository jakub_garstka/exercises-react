import styled from 'styled-components';
import { colors } from '../assets/styles';

const Select = styled.select`
    cursor: pointer;
    width: 100%;
    height: 35px;
    background: ${colors.gray_1};
    color: ${colors.white};
    padding-left: 5px;
    border: none;
    font-weight: 700;
    font-size: 1rem;
    &:hover {
        color: ${colors.blue_0};
    }
    &:focus {
        outline: none;
    }
    option {
        color: black;
        background: white;
        display: flex;
        white-space: pre;
        min-height: 20px;
        padding: 0px 2px 1px;
    }
`;

export default Select;
