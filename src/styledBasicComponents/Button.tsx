import styled from 'styled-components';
import { colors } from '../assets/styles';

export const Button = styled.button`
    cursor: pointer;
    margin: 5px;
    color: ${colors.white};
    position: relative;
    padding: 1rem;
    background: none;
    border: none;
    font-weight: 700;
    transform-style: preserve-3d;
    &:focus {
        outline: none
    };
    &:hover {
         &::before {
            top: 0px;
            right: 0px;
         };
         &::after {
             top: 5px;
             right: 5px;
         };
    };
    &:before {
        transition: top .1s ease-in-out, right .1s ease-in-out;
        background-color: ${colors.blue_1};
        content: "";
        position: absolute;  
        transform: translateZ(1px);
        top: 5px;
        right: 5px;
        width: 100%;
        height: 100%;
        transform: translateZ(-1px);

    };
    &:after {
        transition: top .1s ease-in-out, right .1s ease-in-out;
        content: "";
        position: absolute;    
        outline: 1px solid white;
        transform: translateZ(-1px);
        top: 0px;
        right: 0px;
        width: 100%;
        height: 100%;
    }
}`;
