import React from 'react';
import withProtection from './withProtection';

const ProtectedContent: React.FunctionComponent = () => {
    return <h3>User has access to those resources!</h3>;
};

export default withProtection(ProtectedContent);
