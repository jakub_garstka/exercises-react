import React from 'react';
import { Button } from '../../styledBasicComponents/Button';
import { ProvideProtectedViewContext } from './ProtectionProvider';

const ProtectionSetter: React.FunctionComponent = () => {
    const { setAuth } = ProvideProtectedViewContext();
    return <Button onClick={() => setAuth(false)}>Disable auth and redirect!</Button>;
};

export default ProtectionSetter;
