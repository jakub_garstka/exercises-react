import React, { createContext, useContext, useState } from 'react';

export interface ProtectedViewContext {
    auth: boolean;
    setAuth: (auth: boolean) => void;
}

export const ProtectedViewContext = createContext<ProtectedViewContext | null>(null);

export const ProvideProtectedViewContext = (): ProtectedViewContext => {
    const protectedViewContext = useContext(ProtectedViewContext);
    if (!protectedViewContext) {
        throw new Error('protectedViewContext must be used within a ProtectedViewProvider');
    }
    return protectedViewContext;
};

export const ProtectedViewProvider: React.FunctionComponent = ({ children }) => {
    const [auth, setAuth] = useState(true);
    return <ProtectedViewContext.Provider value={{ auth, setAuth }}>{children}</ProtectedViewContext.Provider>;
};
