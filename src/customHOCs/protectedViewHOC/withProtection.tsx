import React, { ComponentType } from 'react';
import { Redirect } from '@reach/router';
import { ProtectedViewContext } from './ProtectionProvider';

const withProtection = <T,>(Component: ComponentType<T>): ComponentType<T> => (props: T) => (
    <ProtectedViewContext.Consumer>
        {(value: ProtectedViewContext | null) => (value?.auth ? <Component {...props} /> : <Redirect noThrow to="/" />)}
    </ProtectedViewContext.Consumer>
);

export default withProtection;
