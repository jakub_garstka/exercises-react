export const colors = {
    blue_0: '#9B66FF',
    blue_1: '#3500d3',
    blue_2: '#240090',
    blue_3: '#190061',
    blue_4: '#0C0032',
    gray_1: '#282828',
    gray_2: '#4b4b4b',
    white: '#FFFFFF',
};
