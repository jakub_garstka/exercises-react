import Select from '../../styledBasicComponents/Select';
import React, { FunctionComponent } from 'react';
import styled from 'styled-components';
import { colors } from '../../assets/styles';
import { ReactComponent as Next } from '../../assets/icons/arrows/right.svg';
import { ReactComponent as ToFirst } from '../../assets/icons/arrows/left_edge.svg';
import { ReactComponent as Previous } from '../../assets/icons/arrows/left.svg';
import { ReactComponent as ToLast } from '../../assets/icons/arrows/right_edge.svg';

const PaginationWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    flex-flow: row wrap;
    padding: 1rem;
    background-color: ${colors.gray_1};
    ${Select} {
        width: 3rem;
    }
`;

const PageManager = styled.div`
    display: flex;
    font-weight: 700;
    font-size: 1rem;
    flex-direction: row;
    align-items: center;
    color: ${colors.white};
`;

const IconWrapper = styled.span`
        width: 1.5rem;
        padding: 0.1rem;
        height: auto;
        & > svg {
            cursor: pointer;
            fill: ${colors.white};
            &:hover {
                fill: ${colors.blue_0};
            }
        }
    }
`;

interface TablePaginationProps {
    currentPage: number;
    rowsPerPage: number[];
    setRowsPerPage: (rows: number) => void;
    goToNextPage: () => void;
    goToLastPage: () => void;
    goToPreviousPage: () => void;
    goToFirstPage: () => void;
    hasNextPage: boolean;
    hasPreviousPage: boolean;
}

const TablePagination: FunctionComponent<TablePaginationProps> = ({
    currentPage,
    rowsPerPage,
    setRowsPerPage,
    goToNextPage,
    goToPreviousPage,
    goToFirstPage,
    goToLastPage,
    hasNextPage,
    hasPreviousPage,
}) => {
    return (
        <PaginationWrapper>
            <Select onChange={(e: React.ChangeEvent<HTMLSelectElement>): void => setRowsPerPage(+e.target.value)}>
                {rowsPerPage.map((rows) => (
                    <option key={rows} value={rows}>
                        {rows}
                    </option>
                ))}
            </Select>
            <PageManager>
                <IconWrapper>{hasPreviousPage && <ToFirst onClick={() => goToFirstPage()} />}</IconWrapper>
                <IconWrapper>{hasPreviousPage && <Previous onClick={() => goToPreviousPage()} />}</IconWrapper>
                {currentPage}
                <IconWrapper>{hasNextPage && <Next onClick={() => goToNextPage()} />}</IconWrapper>
                <IconWrapper>{hasNextPage && <ToLast onClick={() => goToLastPage()} />}</IconWrapper>
            </PageManager>
        </PaginationWrapper>
    );
};

export default TablePagination;
