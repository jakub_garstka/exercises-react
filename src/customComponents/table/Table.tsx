import styled from 'styled-components';
import { colors } from '../../assets/styles';

export const TableRowAccessories = styled.tr`
    width: 100%;
    background-color: ${colors.gray_1};
    font-weight: 700;
`;

export const TableRowContent = styled.tr`
    width: 100%;
    background-color: ${colors.gray_1};
    transition: background-color 0.3s ease-in-out;
    &:hover {
        background-color: ${colors.gray_2};
    }
`;

interface TableCellProps {
    align?: string;
}

export const TableCell = styled.td<TableCellProps>`
    padding: 1rem;
    text-align: ${(props: TableCellProps) => props.align || 'left'};
`;

export const Table = styled.table`
    display: block;
    border-collapse: collapse;
    position: relative;
    &:after {
        z-index: -1;
        content: '';
        position: absolute;
        border: 1px solid ${colors.white};
        width: 100%;
        height: 100%;
        top: -10px;
        right: -10px;
    }
`;

export const TableBody = styled.tbody`
    display: table;
    width: 100%;
`;

export const TableTitle = styled.h3`
    font-size: 1.5rem;
    font-weight: 700;
    margin: 0;
    padding: 1rem;
    background: ${colors.gray_1};
`;
