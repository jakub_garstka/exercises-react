import styled from 'styled-components';
import { colors } from '../../assets/styles';

export const TaskDescription = styled.p`
    font-size: 1rem;
    word-wrap: normal;
`;

export const TaskTitle = styled.h3`
    font-weight: 700;
`;

export const TaskWrapper = styled.div`
    padding: 1rem 0;
    margin: 1rem 0;
    display: flex;
    flex-direction: column;
    width: 400px;
    position: relative;
    border-bottom: 1px solid ${colors.white};
`;
