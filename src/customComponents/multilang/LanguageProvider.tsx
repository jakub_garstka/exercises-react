import React, { useContext, useState } from 'react';
import { en, Language } from './Translations';

interface LanguageContext {
    language: Language;
    languageSetter: (language: Language) => void;
}

const LangContext = React.createContext<LanguageContext | null>(null);

export const UseLanguageContext = (): LanguageContext => {
    const context = useContext(LangContext);
    if (!context) {
        throw new Error('useLanguageContext must be used within a UserLoginProvider');
    }
    return context;
};

export const LanguageProvider: React.FunctionComponent = ({ children }) => {
    const [language, setLanguage] = useState(en);
    return <LangContext.Provider value={{ language, languageSetter: setLanguage }}>{children}</LangContext.Provider>;
};
