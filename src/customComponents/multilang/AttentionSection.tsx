import React from 'react';
import { Button } from '../../styledBasicComponents/Button';
import { UseLanguageContext } from './LanguageProvider';

const AttentionSection: React.FunctionComponent = () => {
    const { language } = UseLanguageContext();
    const { title, subtitle, ctaButton } = language['attention'];
    return (
        <>
            <h1>{title}</h1>
            <h2>{subtitle}</h2>
            <Button>{ctaButton}</Button>
        </>
    );
};

export default AttentionSection;
