import React from 'react';
import { ReactComponent as PL } from '../../assets/icons/flags/poland.svg';
import { ReactComponent as EN } from '../../assets/icons/flags/england.svg';

export interface Language {
    attention: {
        title: string;
        subtitle: string;
        ctaButton: string;
    };
    newsletter: {
        title: string;
        ctaButton: string;
        action: string;
    };
}

export interface Translation {
    id: string;
    language: Language;
    flagSVG: React.ReactNode;
}

export const pl: Language = {
    attention: {
        title: 'Dobrze, że jesteś, sprawdź to zadanie',
        subtitle: 'Pomoże Ci ogarnąć jak zmieniać język w apkach reacta',
        ctaButton: 'Dowiedź się więcej',
    },
    newsletter: {
        title: 'Bądź na bieżąco',
        ctaButton: 'Idź do repo ->',
        action: '/new-subscriber?lang=pl',
    },
};

export const en: Language = {
    attention: {
        title: 'Hey, check this task',
        subtitle: 'It can help You to learn how to change language in react app',
        ctaButton: 'More',
    },
    newsletter: {
        title: "Let's keep in touch",
        ctaButton: 'To repository !!!',
        action: '/new-subscriber?lang=en',
    },
};

export const translations: Translation[] = [
    { id: 'en', language: en, flagSVG: <EN /> },
    { id: 'pl', language: pl, flagSVG: <PL /> },
];
