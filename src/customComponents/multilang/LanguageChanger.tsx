import React, { FunctionComponent } from 'react';
import styled from 'styled-components';
import { Button } from '../../styledBasicComponents/Button';
import { Translation } from './Translations';
import { UseLanguageContext } from './LanguageProvider';

interface LanguageChangerProps {
    translations: Translation[];
}

const LanguageChanger: FunctionComponent<LanguageChangerProps> = ({ translations }) => {
    const { languageSetter } = UseLanguageContext();

    const ButtonsWrapper = styled.div`
        display: flex;
        padding: 5px;
    `;

    const SvgWrapper = styled.div`
        height: auto;
        width: 40px;
    `;
    return (
        <ButtonsWrapper>
            {translations.map((t) => (
                <Button key={t.id} onClick={() => languageSetter(t.language)}>
                    <SvgWrapper>{t.flagSVG}</SvgWrapper>
                </Button>
            ))}
        </ButtonsWrapper>
    );
};

export default LanguageChanger;
