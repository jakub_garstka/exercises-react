import React from 'react';
import { UseLanguageContext } from './LanguageProvider';

const NewsletterSection: React.FunctionComponent = () => {
    const { language } = UseLanguageContext();
    const { title } = language['newsletter'];
    return (
        <>
            <h1>{title}</h1>
        </>
    );
};

export default NewsletterSection;
