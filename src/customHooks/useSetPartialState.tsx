import { useState } from 'react';

export const useSetPartialState = <T,>(initialState: T): [T, (state: T) => void] => {
    const [innerState, setInnerState] = useState<T>(initialState);
    const setPartialState = (state: T) => {
        setInnerState({
            ...innerState,
            ...state,
        });
    };

    return [innerState, setPartialState];
};
