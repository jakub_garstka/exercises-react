import { useEffect, useRef, useState } from 'react';

interface PaginationContent<T> {
    currentPage: number;
    goToNextPage: () => void;
    goToLastPage: () => void;
    goToPreviousPage: () => void;
    goToFirstPage: () => void;
    setRowsPerPage: (rowsNumber: number) => void;
    elements: T[];
    hasNextPage: boolean;
    hasPreviousPage: boolean;
    isBusy: boolean;
}

export function usePagination<T>(
    dataEntries: T[],
    initialRowsPerPage = 5,
    initialCurrentPage = 1,
): PaginationContent<T> {
    const [allDataEntries] = useState(dataEntries);
    const [rowsPerPage, setRowsPerPage] = useState(initialRowsPerPage);
    const [pagesNumber, setPagesNumber] = useState(Math.ceil(dataEntries.length / rowsPerPage));
    const [currentPage, setCurrentPage] = useState(initialCurrentPage);
    const [elements, setElements] = useState(
        dataEntries.slice((initialCurrentPage - 1) * rowsPerPage, (initialCurrentPage - 1) * rowsPerPage + rowsPerPage),
    );
    const [hasNextPage, setHasNextPage] = useState(true);
    const [hasPreviousPage, setHasPreviousPage] = useState(false);
    const [isBusy, setIsBusy] = useState(true);
    const timeoutRef = useRef(0);
    useEffect((): (() => void) => {
        const tempPagesNumber = Math.ceil(allDataEntries.length / rowsPerPage);
        let start;
        if (tempPagesNumber < currentPage) {
            setHasNextPage(false);
            setHasPreviousPage(tempPagesNumber !== 1);
            setCurrentPage(tempPagesNumber);
            start = (tempPagesNumber - 1) * rowsPerPage;
        } else {
            setHasNextPage(currentPage !== pagesNumber);
            setHasPreviousPage(currentPage !== 1);
            start = (currentPage - 1) * rowsPerPage;
        }
        setPagesNumber(tempPagesNumber);
        setElements(allDataEntries.slice(start, start + rowsPerPage));

        setIsBusy(true);
        clearInterval(timeoutRef.current);
        timeoutRef.current = setTimeout(() => {
            setIsBusy(false);
        }, 3333);

        return () => {
            clearInterval(timeoutRef.current);
        };
    }, [currentPage, rowsPerPage, pagesNumber, allDataEntries]);

    const goToPreviousPage = (): void => setCurrentPage(currentPage - 1);
    const goToFirstPage = (): void => setCurrentPage(1);

    const goToNextPage = (): void => setCurrentPage(currentPage + 1);
    const goToLastPage = (): void => setCurrentPage(pagesNumber);

    return {
        currentPage,
        goToNextPage,
        goToLastPage,
        goToFirstPage,
        goToPreviousPage,
        setRowsPerPage,
        elements,
        hasNextPage,
        hasPreviousPage,
        isBusy,
    };
}
