import { useEffect, useState } from 'react';

interface Environment {
    xSize: number;
    ySize: number;
    isOnline: boolean;
    isTouchScreenEnabled: boolean;
    os: string;
    browser: string;
}

const isTouchScreenEnabled = (): boolean =>
    'ontouchstart' in window || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0;

const findOS = (): string => {
    const agent = navigator.userAgent;
    switch (true) {
        case agent.indexOf('Windows') > -1:
            return 'Windows';
        case agent.indexOf('Mac') > -1:
            return 'Mac/iOS';
        case agent.indexOf('Linux') > -1:
            return 'Linux';
        case agent.indexOf('Android') > -1:
            return 'Android';
        case agent.indexOf('iOS') > -1:
            return 'iOS';
        case agent.indexOf('X11') > -1:
            return 'UNIX';
        default:
            return 'Unknown OS';
    }
};

const findBrowser = (): string => {
    const agent = navigator.userAgent;
    switch (true) {
        case agent.indexOf('edge') > -1:
            return 'edge';
        case agent.indexOf('edg') > -1:
            return 'chromium based edge (dev or canary)';
        case agent.indexOf('opr') > -1:
            return 'opera';
        case agent.indexOf('chrome') > -1:
            return 'chrome';
        case agent.indexOf('trident') > -1:
            return 'ie';
        case agent.indexOf('firefox') > -1:
            return 'firefox';
        case agent.indexOf('safari') > -1:
            return 'safari';
        default:
            return 'Unknown Browser';
    }
};

export const useEnv = () => {
    const initialState: Environment = {
        xSize: window.innerWidth,
        ySize: window.innerHeight,
        isOnline: navigator.onLine,
        isTouchScreenEnabled: isTouchScreenEnabled(),
        os: findOS(),
        browser: findBrowser(),
    };

    const [environment, setEnvironment] = useState(initialState);

    const setWindowSize = () => {
        setEnvironment({
            ...initialState,
            xSize: window.innerWidth,
            ySize: window.innerHeight,
        });
    };

    const setNetworkStatus = () => {
        setEnvironment({
            ...initialState,
            isOnline: navigator.onLine,
        });
    };

    useEffect(() => {
        window.addEventListener('resize', setWindowSize);
        window.addEventListener('offline', setNetworkStatus);
        window.addEventListener('online', setNetworkStatus);
        return () => {
            window.removeEventListener('resize', setWindowSize);
            window.removeEventListener('offline', setNetworkStatus);
            window.removeEventListener('online', setNetworkStatus);
        };
    }, []);
    return environment;
};
