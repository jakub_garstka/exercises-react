import { useState } from 'react';

// It was part of the exercise from the link: https://github.com/Przemocny/zbior-zadan-html-css-js-react/blob/master/REACT/useMemoState.js
// For me it is completely useless and not intuitive due to the fact that useState works out of the box like in given description
export const useMemoState = <T,>(initialState: T): [T, (state: T) => void] => {
    const [innerState, setInnerState] = useState<T>(initialState);
    const compareAndSetState = (state: T) => {
        if (JSON.stringify(state) !== JSON.stringify(innerState)) {
            setInnerState(state);
        }
    };
    return [innerState, (state: T) => compareAndSetState(state)];
};
