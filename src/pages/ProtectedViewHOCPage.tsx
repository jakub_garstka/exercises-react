import React from 'react';
import { RouteComponentProps } from '@reach/router';
import { ProtectedViewProvider } from '../customHOCs/protectedViewHOC/ProtectionProvider';
import ProtectionSetter from '../customHOCs/protectedViewHOC/ProtectionSetter';
import ProtectedContent from '../customHOCs/protectedViewHOC/ProtectedContent';
import { TaskDescription, TaskTitle, TaskWrapper } from '../customComponents/taskDescription/TaskDescription';
import { CenteredContainer } from '../styledBasicComponents/CenteredContainer';

const ProtectedViewHOCPage: React.FunctionComponent<RouteComponentProps> = () => {
    return (
        <CenteredContainer>
            <TaskWrapper>
                <TaskTitle>COŚ</TaskTitle>
                <TaskDescription>
                    fskdafkdsakh fjkshgjk shdakj ghdksjhg kjsdh kjghsdjk gkjs bdjkb jgsdbkjgb jksdbjkg bsdjkbgj ksdbg
                    jkbsdjkgbjksdb gsdbagsd dafgadsgsdagadgsadbg sgk sbdk ghdsh gkjshjkhgk shdkg s
                </TaskDescription>
            </TaskWrapper>
            <ProtectedViewProvider>
                <ProtectionSetter />
                <ProtectedContent />
            </ProtectedViewProvider>
        </CenteredContainer>
    );
};

export default ProtectedViewHOCPage;
