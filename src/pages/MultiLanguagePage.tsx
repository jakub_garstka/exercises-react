import React from 'react';
import { RouteComponentProps } from '@reach/router';
import { LanguageProvider } from '../customComponents/multilang/LanguageProvider';
import AttentionSection from '../customComponents/multilang/AttentionSection';
import NewsletterSection from '../customComponents/multilang/NewsletterSection';
import LanguageChanger from '../customComponents/multilang/LanguageChanger';
import { translations } from '../customComponents/multilang/Translations';
import { TaskDescription, TaskTitle, TaskWrapper } from '../customComponents/taskDescription/TaskDescription';
import { CenteredContainer } from '../styledBasicComponents/CenteredContainer';

export const MultiLanguagePage: React.FunctionComponent<RouteComponentProps> = () => {
    return (
        <CenteredContainer>
            <TaskWrapper>
                <TaskTitle>COŚ</TaskTitle>
                <TaskDescription>
                    fskdafkdsakh fjkshgjk shdakj ghdksjhg kjsdh kjghsdjk gkjs bdjkb jgsdbkjgb jksdbjkg bsdjkbgj ksdbg
                    jkbsdjkgbjksdb gsdbagsd dafgadsgsdagadgsadbg sgk sbdk ghdsh gkjshjkhgk shdkg s
                </TaskDescription>
            </TaskWrapper>
            <LanguageProvider>
                <LanguageChanger translations={translations} />
                <AttentionSection />
                <NewsletterSection />
            </LanguageProvider>
        </CenteredContainer>
    );
};
