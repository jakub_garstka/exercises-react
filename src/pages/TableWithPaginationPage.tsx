import React, { FunctionComponent } from 'react';
import { RouteComponentProps } from '@reach/router';
import {
    TableCell,
    TableRowAccessories,
    TableRowContent,
    Table,
    TableBody,
    TableTitle,
} from '../customComponents/table/Table';
import TablePagination from '../customComponents/table/TablePagination';
import { usePagination } from '../customHooks/usePagination';
import styled from 'styled-components';
import { TaskDescription, TaskTitle, TaskWrapper } from '../customComponents/taskDescription/TaskDescription';
import { CenteredContainer } from '../styledBasicComponents/CenteredContainer';

const LongCell = styled(TableCell)`
    width: 50%;
`;

const ShortCell = styled(TableCell)`
    width: 25%;
`;

const TableWrapper = styled.div`
    width: 500px;
`;

const BusyWrapper = styled.div`
    text-align: center;
`;

const TableWithPaginationPage: FunctionComponent<RouteComponentProps> = () => {
    function createData(name: string, calories: number, fat: number) {
        return { name, calories, fat };
    }

    const rows = [
        createData('Cupcake', 305, 3.7),
        createData('Donut', 452, 25.0),
        createData('Eclair', 262, 16.0),
        createData('Frozen yoghurt', 159, 6.0),
        createData('Gingerbread', 356, 16.0),
        createData('Honeycomb', 408, 3.2),
        createData('Ice cream sandwich', 237, 9.0),
        createData('Jelly Bean', 375, 0.0),
        createData('KitKat', 518, 26.0),
        createData('Lollipop', 392, 0.2),
        createData('Marshmallow', 318, 0),
        createData('Nougat', 360, 19.0),
        createData('Oreo', 437, 18.0),
        createData('Cupcake', 305, 3.7),
        createData('Donut', 452, 25.0),
        createData('Eclair', 262, 16.0),
        createData('Frozen yoghurt', 159, 6.0),
        createData('Gingerbread', 356, 16.0),
        createData('Honeycomb', 408, 3.2),
        createData('Ice cream sandwich', 237, 9.0),
        createData('Jelly Bean', 375, 0.0),
        createData('KitKat', 518, 26.0),
        createData('Lollipop', 392, 0.2),
        createData('Marshmallow', 318, 0),
        createData('Nougat', 360, 19.0),
        createData('Oreo', 437, 18.0),
        createData('Cupcake', 305, 3.7),
        createData('Donut', 452, 25.0),
        createData('Eclair', 262, 16.0),
        createData('Frozen yoghurt', 159, 6.0),
        createData('Gingerbread', 356, 16.0),
        createData('Honeycomb', 408, 3.2),
        createData('Ice cream sandwich', 237, 9.0),
        createData('Jelly Bean', 375, 0.0),
        createData('KitKat', 518, 26.0),
        createData('Lollipop', 392, 0.2),
        createData('Marshmallow', 318, 0),
        createData('Nougat', 360, 19.0),
        createData('Oreo', 437, 18.0),
    ];

    const rowsPerPage = [5, 10, 15, 20];

    const {
        currentPage,
        goToNextPage,
        goToLastPage,
        goToFirstPage,
        goToPreviousPage,
        setRowsPerPage,
        elements,
        hasNextPage,
        hasPreviousPage,
        isBusy,
    } = usePagination(rows, 5);

    return (
        <CenteredContainer>
            <TaskWrapper>
                <TaskTitle>COŚ</TaskTitle>
                <TaskDescription>
                    fskdafkdsakh fjkshgjk shdakj ghdksjhg kjsdh kjghsdjk gkjs bdjkb jgsdbkjgb jksdbjkg bsdjkbgj ksdbg
                    jkbsdjkgbjksdb gsdbagsd dafgadsgsdagadgsadbg sgk sbdk ghdsh gkjshjkhgk shdkg s
                </TaskDescription>
            </TaskWrapper>
            <TableWrapper>
                {isBusy ? (
                    <BusyWrapper>BUSY!</BusyWrapper>
                ) : (
                    <Table>
                        <TableBody>
                            <TableRowAccessories>
                                <TableCell colSpan={3}>
                                    <TableTitle>Table With Pagination</TableTitle>
                                </TableCell>
                            </TableRowAccessories>
                            <TableRowAccessories>
                                <LongCell>Name</LongCell>
                                <ShortCell>Calories</ShortCell>
                                <ShortCell>Fat&nbsp;(g)</ShortCell>
                            </TableRowAccessories>
                            {elements.map((element) => (
                                <TableRowContent key={element.name}>
                                    <LongCell>{element.name}</LongCell>
                                    <ShortCell align="right">{element.calories}</ShortCell>
                                    <ShortCell align="right">{element.fat}</ShortCell>
                                </TableRowContent>
                            ))}
                            <TableRowAccessories>
                                <TableCell colSpan={3}>
                                    <TablePagination
                                        currentPage={currentPage}
                                        rowsPerPage={rowsPerPage}
                                        setRowsPerPage={setRowsPerPage}
                                        goToNextPage={goToNextPage}
                                        goToPreviousPage={goToPreviousPage}
                                        goToLastPage={goToLastPage}
                                        goToFirstPage={goToFirstPage}
                                        hasNextPage={hasNextPage}
                                        hasPreviousPage={hasPreviousPage}
                                    />
                                </TableCell>
                            </TableRowAccessories>
                        </TableBody>
                    </Table>
                )}
            </TableWrapper>
        </CenteredContainer>
    );
};

export default TableWithPaginationPage;
