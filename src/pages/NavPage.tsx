import React from 'react';
import { Link, RouteComponentProps } from '@reach/router';
import styled from 'styled-components';
import { colors } from '../assets/styles';

const NavWrapper = styled.nav`
    display: flex;
    flex-flow: column wrap;
`;

const StyledLink = styled(Link)`
    color: ${colors.white};
    font-size: 2rem;
    text-decoration: none;
    padding: 1rem;
    transition: color 0.3s ease-in-out;
    &:hover {
        color: ${colors.blue_1};
    }
`;

const NavPage: React.FunctionComponent<RouteComponentProps> = () => {
    return (
        <NavWrapper>
            <StyledLink to="multi_language_page">Multi Language Page</StyledLink>
            <StyledLink to="use_set_partial_state_page">useSetPartialState Page</StyledLink>
            <StyledLink to="protected_view_hoc_page">protectedViewHOC Page</StyledLink>
            <StyledLink to="table_with_pagination_page">Table With Pagination Page</StyledLink>
        </NavWrapper>
    );
};

export default NavPage;
