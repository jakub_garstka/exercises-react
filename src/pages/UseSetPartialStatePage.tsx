import React from 'react';
import { RouteComponentProps } from '@reach/router';
import styled from 'styled-components';
import { colors } from '../assets/styles';
import { Button } from '../styledBasicComponents/Button';
import { useSetPartialState } from '../customHooks/useSetPartialState';
import ReactDiffViewer, { ReactDiffViewerStylesOverride } from 'react-diff-viewer';
import { TaskDescription, TaskTitle, TaskWrapper } from '../customComponents/taskDescription/TaskDescription';
import { CenteredContainer } from '../styledBasicComponents/CenteredContainer';

const INITIAL_STATE: unknown = {
    test: 1,
    language: 'english',
    exercises: {
        first: {
            points: 5,
            max: 10,
        },
    },
};

const UPDATED_PARTIAL_STATE: unknown = {
    exercises: {
        first: {
            points: 5,
            max: 10,
            min: 2,
        },
    },
};

const newStyles: ReactDiffViewerStylesOverride | undefined = {
    variables: {
        dark: {
            diffViewerBackground: colors.gray_1,
            gutterColor: colors.white,
        },
    },
};

const ButtonWrapper = styled.div`
    display: flex;
    flex-flow: row wrap;
    justify-content: space-evenly;
    margin: 10px;
`;

const ReactDiffViewerWrapper = styled.div`
    position: relative;
    margin: 20px;
    &:after {
        z-index: -1;
        content: '';
        position: absolute;
        border: 1px solid ${colors.white};
        width: 100%;
        height: 100%;
        top: -10px;
        right: -10px;
    }
`;

const UseSetPartialStatePage: React.FunctionComponent<RouteComponentProps> = () => {
    const [state, setPartialState] = useSetPartialState(INITIAL_STATE);

    return (
        <CenteredContainer>
            <TaskWrapper>
                <TaskTitle>COŚ</TaskTitle>
                <TaskDescription>
                    fskdafkdsakh fjkshgjk shdakj ghdksjhg kjsdh kjghsdjk gkjs bdjkb jgsdbkjgb jksdbjkg bsdjkbgj ksdbg
                    jkbsdjkgbjksdb gsdbagsd dafgadsgsdagadgsadbg sgk sbdk ghdsh gkjshjkhgk shdkg s
                </TaskDescription>
            </TaskWrapper>
            <ButtonWrapper>
                <Button onClick={(): void => setPartialState(UPDATED_PARTIAL_STATE)}>Change!</Button>
                <Button onClick={(): void => setPartialState(INITIAL_STATE)}>Reset!</Button>
            </ButtonWrapper>
            <ReactDiffViewerWrapper>
                <ReactDiffViewer
                    leftTitle="Initial State"
                    rightTitle="State after changes"
                    oldValue={JSON.stringify(INITIAL_STATE, null, '\t')}
                    newValue={JSON.stringify(state, null, '\t')}
                    splitView={true}
                    useDarkTheme={true}
                    showDiffOnly={false}
                    styles={newStyles}
                />
            </ReactDiffViewerWrapper>
        </CenteredContainer>
    );
};

export default UseSetPartialStatePage;
