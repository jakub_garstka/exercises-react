import React from 'react';
import './App.css';
import { useEnv } from './customHooks/useEnv';
import { Router } from '@reach/router';
import NavPage from './pages/NavPage';
import styled from 'styled-components';
import { MultiLanguagePage } from './pages/MultiLanguagePage';
import UseSetPartialStatePage from './pages/UseSetPartialStatePage';
import ProtectedViewHOCPage from './pages/ProtectedViewHOCPage';
import TableWithPaginationPage from './pages/TableWithPaginationPage';

const PageContent = styled.div`
    display: flex;
    justify-content: center;
    margin-top: 100px;
    // min-height: 100vh;
`;

function App() {
    const env = useEnv();
    return (
        <PageContent>
            <Router>
                <NavPage default />
                <MultiLanguagePage path="multi_language_page" />
                <UseSetPartialStatePage path="use_set_partial_state_page" />
                <ProtectedViewHOCPage path="protected_view_hoc_page" />
                <TableWithPaginationPage path="table_with_pagination_page" />
            </Router>
        </PageContent>
    );
}

export default App;
