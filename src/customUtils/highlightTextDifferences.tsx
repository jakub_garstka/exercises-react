import React from 'react';

export const highlightTextDifferences = (oldText: string, newText: string, color: string, separator = '') => {
    const oldWords = oldText.split(separator);
    const newWords = newText.split(separator);

    return newWords.map((value, index) => {
        const tempSeparator = index === newWords.length - 1 ? '' : separator;
        return value === oldWords[index] ? (
            value + tempSeparator
        ) : (
            <span style={{ color: color }}>{value + tempSeparator}</span>
        );
    });
};
